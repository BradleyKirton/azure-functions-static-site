from __future__ import annotations

import pathlib
import logging
import mimetypes
import azure.functions as func


def main(request: func.HttpRequest, context: func.Context) -> func.HttpResponse:
    """Serve the requested static file."""

    logging.info("Python HTTP trigger function processed a request.")

    status_code = 200
    path = request.params.get("path", "index.html")
    content_type = mimetypes.guess_type(path)[0]

    # Get the static file
    function_path = pathlib.Path(context.function_directory)
    static_path = function_path.joinpath("static")
    static_file_path = static_path.joinpath(path)

    if static_file_path.exists() is False:
        status_code = 404
        static_file_path = static_path.joinpath("404.html")
        content_type = "text/html"

    with open(static_file_path, "rb") as stream:
        return func.HttpResponse(
            stream.read(),
            status_code=status_code,
            mimetype=content_type,
            headers={"Cache-Control": "max-age-300"},
        )
