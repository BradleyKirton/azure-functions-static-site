# Azure Functions Static Site

This repository provides an example of how it is possible to serve a static site using Azure functions. For a detailed write up check out the accompanying [blog post](https://bradleykirton.com/blog/serving-static-site-azure-functions/).

![alt text](screenshots/azure-static-site.png "Example site")

# Project Structure

```bash
├── __app__
│   ├── __init__.py
│   ├── host.json
│   ├── local.settings.json
│   ├── poetry.lock
│   ├── proxies.json
│   ├── pyproject.toml
│   └── static_file_server
│       ├── function.json
│       ├── __init__.py
│       └── static
│           ├── 404.html
│           └── index.html
├── docker-compose.yml
├── Dockerfile
├── Makefile
└── README.md
```

This project makes use of Docker and docker compose to run the functions locally. The docker-compose file is provided to local development as it contains the Azurite storage emulator. Azurite emulates a storage account which is required for the functions to run.

The project contains a single function `static_file_server`. This function is available at `/api/static_file_server` and requires a single query parameter `path`. This path is a file path from the `static` directory within the `static_file_server` function directory.

The project applies a `proxy` which proxies requests from the root path to the `static_file_server`.

# Run Locally

A Makefile is provided for convenience and contains the targets required to manage the project locally.

## Build the project

Before you can build the project locally you must create a `.private.env` file. The purpose of this file is to locally store all private environment variables. Note this file is excluded from the git repository.

```bash
make build
```

## Bring up containers

```bash
make up
```

## Bring down containers

```bash
make down
```

# Proxy

The project applies a proxy which proxies all requests from the root path to our static file server function. Note the **APPLICATION_HOST** environment variable in the `proxies.json` file. When developing locally this variable is set to **localhost**. When you deploy your file server function you will need to add the function host to this environment variable under the `Configuration` blade on the function detail page of the Azure portal.

```json
{
  "proxies": {
    "assets": {
      "matchCondition": {
        "route": "{*path}"
      },
      "backendUri": "https://%APPLICATION_HOST%/api/static_file_server?path={path}"
    }
  }
}
```