FROM mcr.microsoft.com/azure-functions/python:3.0-python3.8

ENV AzureWebJobsScriptRoot=/home/site/wwwroot \
    AzureFunctionsJobHost__Logging__Console__IsEnabled=true

WORKDIR /home/site/wwwroot

# <start:Application Dependencies>
RUN pip install --upgrade pip
RUN pip install poetry

COPY ./__app__/pyproject.toml .
COPY ./__app__/poetry.lock .

RUN poetry config virtualenvs.create false
RUN poetry install
# <end:Application Dependencies>

COPY __app__ .